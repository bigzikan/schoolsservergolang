package main

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

//const connStr string = "user=postgres password=root dbname=SCHOOLS_DB sslmode=enable"

//запись о школьнике
type StudentFromDB struct {
	ID        int    `json:"id"`
	FIO       int    `json:"fio"`
	Birthdate string `json:"birthdate"`
	Address   string `json:"address"`
}

//возвращает ученика по ID
func GetStudentByID(ginContext *gin.Context) {
	Students := []StudentFromDB{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT * FROM public.\"STUDENTS\" WHERE \"ID\" = " + ginContext.Param("id"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		Student := StudentFromDB{}
		err := rows.Scan(&Student.ID, &Student.FIO, &Student.Birthdate, &Student.Address)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Students = append(Students, Student)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Students})
}

//запись о школьнике
type StudentFromSchool struct {
	FIO       int    `json:"fio"`
	Birthdate string `json:"birthdate"`
	Address   string `json:"address"`
	Number    string `json:"number"`
	Liter     string `json:"liter"`
}

//возвращает cписок учеников в школе на данный момент
func GetStudentsBySchoolID(ginContext *gin.Context) {
	Students := []StudentFromSchool{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("select \"STUDENTS\".\"FIO\", \"STUDENTS\".\"BIRTHDATE\", " +
		"\"STUDENTS\".\"ADDRESS\", \"CLASSES\".\"NUMBER\", \"CLASSES\".\"LITER\" " +
		"from \"STUDENTS\" " +
		"left join \"CLASSES_STUDENTS\" ON \"CLASSES_STUDENTS\".\"STUDENTS_ID\" = \"STUDENTS\".\"ID\" " +
		"left join \"SCHOOL_CLASSES\" ON \"SCHOOL_CLASSES\".\"CLASS_ID\" = \"CLASSES_STUDENTS\".\"CLASSES_ID\" " +
		"left join \"CLASSES\" ON \"CLASSES\".\"ID\" = \"CLASSES_STUDENTS\".\"CLASSES_ID\" " +
		"where \"SCHOOL_CLASSES\".\"SCHOOL_ID\" = " + ginContext.Param("id"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		Student := StudentFromSchool{}
		err := rows.Scan(&Student.FIO, &Student.Birthdate, &Student.Address,
			Student.Number, Student.Liter)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Students = append(Students, Student)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Students})
}

type StudentToDB struct {
	FIO       int    `json:"fio"`
	Birthdate string `json:"birthdate"`
	Address   string `json:"address"`
}

//добавляет ученика в БД
func AddNewStudent(ginContext *gin.Context) {
	// Validate input
	var input StudentToDB
	if err := ginContext.ShouldBindJSON(&input); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	// Insert School
	_, err = db.Exec("INSERT INTO public.\"STUDENTS\"(\"FIO\", \"BIRTHDATE\", \"ADDRESS\")"+
		" VALUES ($1, $2, $3)", input.FIO, input.Birthdate, input.Address)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ginContext.JSON(http.StatusOK, gin.H{"data": input})
}
