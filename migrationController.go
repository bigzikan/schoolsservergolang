package main

import (
	"database/sql"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

//const connStr string = "user=postgres password=root dbname=SCHOOLS_DB sslmode=enable"

type StudentMigration struct {
	SchoolID  int    `json:"schoolid"`
	ClassID   int    `json:"classid"`
	StudentID int    `json:"studentid"`
	Date      string `json:"date"`
}

//зачислить ученика
func AdmissionStudent(ginContext *gin.Context) {
	// Validate input
	var input StudentMigration
	if err := ginContext.ShouldBindJSON(&input); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	//добавить запись о зачислении
	_, err = db.Exec("INSERT INTO public.\"HISTORY\"(\"SCHOOL_ID\", \"CLASS_ID\", \"STUDENT_ID\", "+
		"\"ADMISSION_DATE\", \"DISMISSIAL_DATE\") VALUES "+
		"($1, $2, $3, $4, null)", input.SchoolID, input.ClassID, input.StudentID, input.Date)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//добавить связь в таблицу связей
	_, err = db.Exec("INSERT INTO public.\"CLASSES_STUDENTS\"("+
		"\"CLASSES_ID\", \"STUDENTS_ID\") "+
		"VALUES($1, $2)", input.ClassID, input.StudentID)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ginContext.JSON(http.StatusOK, gin.H{"data": input})
}

//отчислить ученика
func DismissialStudent(ginContext *gin.Context) {
	// Validate input
	var input StudentMigration
	if err := ginContext.ShouldBindJSON(&input); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	//узнаем дату зачисления, чтобы новая была больше её или равна
	rows, err := db.Query("SELECT \"HISTORY\".\"ADMISSION_DATE\" " +
		"FROM public.\"HISTORY\" " +
		"WHERE \"SCHOOL_ID\" = " + ginContext.Param("schoolid") + " " +
		"AND \"CLASS_ID\" = " + ginContext.Param("classid") + " " +
		"AND \"STUDENT_ID\" = " + ginContext.Param("studentid") + " " + ";")
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	dateAdmission, _ := time.Parse("2006-01-02", "1899-12-31")
	dateDismissial, _ := time.Parse("2006-01-02", "1899-12-31")
	for rows.Next() {
		admissionDate := ""
		err := rows.Scan(&admissionDate)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		dateAdmission, _ = time.Parse("2006-01-02", admissionDate)
	}
	dateDismissial, _ = time.Parse("2006-01-02", input.Date)

	if dateAdmission.After(dateDismissial) {
		ginContext.JSON(http.StatusBadRequest,
			gin.H{"error": "Дата отчисления не должна быть меньше даты зачисления!"})
		return
	}

	//обновить дату отчисления
	_, err = db.Exec("UPDATE public.\"HISTORY\" "+
		"SET \"DISMISSIAL_DATE\" = $1 "+
		"WHERE \"SCHOOL_ID\" = $2 "+
		"AND \"CLASS_ID\" = $3 "+
		"AND \"STUDENT_ID\" = $4)",
		input.Date, input.SchoolID, input.ClassID, input.StudentID)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//удалить связь из таблицы связей
	_, err = db.Exec("DELETE FROM public.\"CLASSES_STUDENTS\" "+
		"WHERE \"CLASSES_ID\" = $1 "+
		"AND \"STUDENTS_ID\" = $2 ",
		input.ClassID, input.StudentID)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ginContext.JSON(http.StatusOK, gin.H{"data": input})
}

type HistoryMigrations struct {
	ID             int    `json:"id"`
	ClassID        int    `json:"classid"`
	StudentID      int    `json:"studentid"`
	AdmissionDate  string `json:"admissiondate"`
	DismissialDate string `json:"dismissialdate"`
	SchoolID       int    `json:"schoolid"`
}

//Информация об учениках за период
func GetStudentsByPeriod(ginContext *gin.Context) {
	Migrations := []HistoryMigrations{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT \"STUDENTS\".* " +
		"FROM public.\"HISTORY\" " +
		"LEFT JOIN \"STUDENTS\" ON \"STUDENTS\".\"ID\" = \"HISTORY\".\"STUDENT_ID\" " +
		"WHERE \"ADMISSION_DATE\" <= " + "'" + ginContext.Param("admissiondate") + "'" + " " +
		"AND (\"DISMISSIAL_DATE\" >= " + "'" + ginContext.Param("dismissialdate") + "'" +
		" OR \"DISMISSIAL_DATE\" IS NULL) " +
		"AND \"HISTORY\".\"CLASS_ID\" = " + ginContext.Param("classid") + " " +
		"AND \"HISTORY\".\"SCHOOL_ID\" = " + ginContext.Param("schoolid"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		Migration := HistoryMigrations{}
		err := rows.Scan(&Migration.ID, &Migration.ClassID, &Migration.StudentID,
			&Migration.AdmissionDate, &Migration.DismissialDate, &Migration.SchoolID)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Migrations = append(Migrations, Migration)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Migrations})
}
