package main

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

//const connStr string = "user=postgres password=root dbname=SCHOOLS_DB sslmode=enable"

//структура школы
type SchoolFromDB struct {
	ID      int    `json:"id"`
	Name    string `json:"name"`
	Address string `json:"address"`
}

//возвращает список школ
func GetSchools(ginContext *gin.Context) {
	Schools := []SchoolFromDB{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT * FROM public.\"SHOOLS\" ")
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		School := SchoolFromDB{}
		err := rows.Scan(&School.ID, &School.Name, &School.Address)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Schools = append(Schools, School)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Schools})
}

//возвращает школу по ID
func GetSchoolByID(ginContext *gin.Context) {
	Schools := []SchoolFromDB{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT * FROM public.\"SHOOLS\" WHERE \"ID\" = " + ginContext.Param("id"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		School := SchoolFromDB{}
		err := rows.Scan(&School.ID, &School.Name, &School.Address)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Schools = append(Schools, School)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Schools})
}

type SchoolToDB struct {
	Name    string `json:"name"`    // binding:"required"`
	Address string `json:"address"` // binding:"required"`
}

func AddNewSchool(ginContext *gin.Context) {
	// Validate input
	var input SchoolToDB
	if err := ginContext.ShouldBindJSON(&input); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	// Insert School
	_, err = db.Exec("INSERT INTO public.\"SHOOLS\"(\"NAME\", \"ADDRESS\") VALUES ($1, $2)",
		input.Name, input.Address)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ginContext.JSON(http.StatusOK, gin.H{"data": input})
}
