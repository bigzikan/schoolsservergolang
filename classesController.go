package main

import (
	"database/sql"
	"net/http"

	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

//const connStr string = "user=postgres password=root dbname=SCHOOLS_DB sslmode=enable"

//структура класса
type ClassFromDB struct {
	ID     int    `json:"id"`
	Number string `json:"number"`
	Liter  string `json:"liter"`
}

//возвращает класс по ID
func GetClassByID(ginContext *gin.Context) {
	Classes := []ClassFromDB{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT * FROM public.\"CLASSES\" WHERE \"ID\" = " + ginContext.Param("id"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		Class := ClassFromDB{}
		err := rows.Scan(&Class.ID, &Class.Number, &Class.Liter)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Classes = append(Classes, Class)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Classes})
}

//возвращает cписок классов в школе
func GetClassessBySchoolID(ginContext *gin.Context) {
	Classes := []ClassFromDB{}
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT \"CLASSES\".* " +
		"FROM public.\"CLASSES\" " +
		"Left join \"SCHOOL_CLASSES\" on \"SCHOOL_CLASSES\".\"CLASS_ID\" = \"CLASSES\".\"ID\"  " +
		"Where \"SCHOOL_CLASSES\".\"SCHOOL_ID\" = " + ginContext.Param("id"))
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer rows.Close()

	for rows.Next() {
		Class := ClassFromDB{}
		err := rows.Scan(&Class.ID, &Class.Number, &Class.Liter)
		if err != nil {
			ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		Classes = append(Classes, Class)
	}
	ginContext.JSON(http.StatusOK, gin.H{"data": Classes})
}

type ClassToDB struct {
	Number string `json:"number"` // binding:"required"`
	Liter  string `json:"liter"`  // binding:"required"`
}

func AddNewClass(ginContext *gin.Context) {
	// Validate input
	var input ClassToDB
	if err := ginContext.ShouldBindJSON(&input); err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db, err := sql.Open("postgres", connStr)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	defer db.Close()

	// Insert School
	_, err = db.Exec("INSERT INTO public.\"CLASSES\"(\"NUMBER\", \"LITER\") VALUES ('$1','&2')",
		input.Number, input.Liter)
	if err != nil {
		ginContext.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	ginContext.JSON(http.StatusOK, gin.H{"data": input})
}
