package main

import (
	"github.com/gin-gonic/gin"
)

const connStr string = "user=postgres password=root dbname=SCHOOLS_DB sslmode=disable"

func main() {
	r := gin.Default()
	//Schools
	r.GET("/api/Schools/GetAllSchools", GetSchools)
	r.GET("/api/Schools/GetSchoolByID/:id", GetSchoolByID)
	r.POST("/api/Schools", AddNewSchool)
	//Classes
	r.GET("/api/Classes/GetClassByID/:id", GetClassByID)
	r.GET("/api/Classes/GetClassessBySchoolID/:id", GetClassessBySchoolID)
	r.POST("/api/Classes", AddNewClass)
	//Students
	r.GET("/api/Students/GetStudentByID/:id", GetStudentByID)
	r.GET("/api/Students/GetStudentsBySchoolID/:id", GetStudentsBySchoolID)
	r.POST("/api/Students", AddNewStudent)
	//Migrations
	r.POST("/api/AdmissionStudent", AdmissionStudent)
	r.POST("/api/DismissialStudent", DismissialStudent)
	r.POST("/api/GetStudentsByPeriod", GetStudentsByPeriod)

	r.Run(":44308")
}
